﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    // Default unlock status for levels.
    bool[] levels = {true, false, false, false, false,
                     false, false, false, false, false,
                     false, false, false, false, false,
                     false, false, false, false, false,
                     false, false, false, false, false};

    // Sets PlayerPrefs level unlock status using bool to int tertiary conditionals.
    // If player has not unlocked level 2, then only level 1 is unlocked.
    // If player has unlocked level 2, then load unlock status from PlayerPrefs.
    private void Awake()
    {
        // If level 2 is not unlocked, set default level unlocks
        if (PlayerPrefs.GetInt("Level02Unlocked") == 0)
        {
            PlayerPrefs.SetInt("Level01Unlocked", levels[0] ? 1 : 0);
            PlayerPrefs.SetInt("Level02Unlocked", levels[1] ? 1 : 0);
            PlayerPrefs.SetInt("Level03Unlocked", levels[2] ? 1 : 0);
            PlayerPrefs.SetInt("Level04Unlocked", levels[3] ? 1 : 0);
            PlayerPrefs.SetInt("Level05Unlocked", levels[4] ? 1 : 0);
            PlayerPrefs.SetInt("Level06Unlocked", levels[5] ? 1 : 0);
            PlayerPrefs.SetInt("Level07Unlocked", levels[6] ? 1 : 0);
            PlayerPrefs.SetInt("Level08Unlocked", levels[7] ? 1 : 0);
            PlayerPrefs.SetInt("Level09Unlocked", levels[8] ? 1 : 0);
            PlayerPrefs.SetInt("Level10Unlocked", levels[9] ? 1 : 0);
            PlayerPrefs.SetInt("Level11Unlocked", levels[10] ? 1 : 0);
            PlayerPrefs.SetInt("Level12Unlocked", levels[11] ? 1 : 0);
            PlayerPrefs.SetInt("Level13Unlocked", levels[12] ? 1 : 0);
            PlayerPrefs.SetInt("Level14Unlocked", levels[13] ? 1 : 0);
            PlayerPrefs.SetInt("Level15Unlocked", levels[14] ? 1 : 0);
            PlayerPrefs.SetInt("Level16Unlocked", levels[15] ? 1 : 0);
            PlayerPrefs.SetInt("Level17Unlocked", levels[16] ? 1 : 0);
            PlayerPrefs.SetInt("Level18Unlocked", levels[17] ? 1 : 0);
            PlayerPrefs.SetInt("Level19Unlocked", levels[18] ? 1 : 0);
            PlayerPrefs.SetInt("Level20Unlocked", levels[19] ? 1 : 0);
            PlayerPrefs.SetInt("Level21Unlocked", levels[20] ? 1 : 0);
            PlayerPrefs.SetInt("Level22Unlocked", levels[21] ? 1 : 0);
            PlayerPrefs.SetInt("Level23Unlocked", levels[22] ? 1 : 0);
            PlayerPrefs.SetInt("Level24Unlocked", levels[23] ? 1 : 0);
            PlayerPrefs.SetInt("Level25Unlocked", levels[24] ? 1 : 0);

            PlayerPrefs.Save();
        } // If level 2 is unlocked, load level progress from playerPrefs
        else
        {
            levels[0] = PlayerPrefs.GetInt("Level01Unlocked") == 1 ? true : false;
            levels[1] = PlayerPrefs.GetInt("Level02Unlocked") == 1 ? true : false;
            levels[2] = PlayerPrefs.GetInt("Level03Unlocked") == 1 ? true : false;
            levels[3] = PlayerPrefs.GetInt("Level04Unlocked") == 1 ? true : false;
            levels[4] = PlayerPrefs.GetInt("Level05Unlocked") == 1 ? true : false;
            levels[5] = PlayerPrefs.GetInt("Level06Unlocked") == 1 ? true : false;
            levels[6] = PlayerPrefs.GetInt("Level07Unlocked") == 1 ? true : false;
            levels[7] = PlayerPrefs.GetInt("Level08Unlocked") == 1 ? true : false;
            levels[8] = PlayerPrefs.GetInt("Level09Unlocked") == 1 ? true : false;
            levels[9] = PlayerPrefs.GetInt("Level10Unlocked") == 1 ? true : false;
            levels[10] = PlayerPrefs.GetInt("Level11Unlocked") == 1 ? true : false;
            levels[11] = PlayerPrefs.GetInt("Level12Unlocked") == 1 ? true : false;
            levels[12] = PlayerPrefs.GetInt("Level13Unlocked") == 1 ? true : false;
            levels[13] = PlayerPrefs.GetInt("Level14Unlocked") == 1 ? true : false;
            levels[14] = PlayerPrefs.GetInt("Level15Unlocked") == 1 ? true : false;
            levels[15] = PlayerPrefs.GetInt("Level16Unlocked") == 1 ? true : false;
            levels[16] = PlayerPrefs.GetInt("Level17Unlocked") == 1 ? true : false;
            levels[17] = PlayerPrefs.GetInt("Level18Unlocked") == 1 ? true : false;
            levels[18] = PlayerPrefs.GetInt("Level19Unlocked") == 1 ? true : false;
            levels[19] = PlayerPrefs.GetInt("Level20Unlocked") == 1 ? true : false;
            levels[20] = PlayerPrefs.GetInt("Level21Unlocked") == 1 ? true : false;
            levels[21] = PlayerPrefs.GetInt("Level22Unlocked") == 1 ? true : false;
            levels[22] = PlayerPrefs.GetInt("Level23Unlocked") == 1 ? true : false;
            levels[23] = PlayerPrefs.GetInt("Level24Unlocked") == 1 ? true : false;
            levels[24] = PlayerPrefs.GetInt("Level25Unlocked") == 1 ? true : false;
        }

    }

    // Set timescale to 1 to prevent locked game.
    // Makes cursor unlocked and visible.
    private void Start()
    {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Exits game completely.
    public void ExitGame()
    {
        Application.Quit();
    }

    // Opens level select menu.
    public void OpenLevelSelect()
    {
        SceneManager.LoadScene("LevelSelectMenu");
    }

    // Opens option menu.
    public void OpenOptions()
    {
        SceneManager.LoadScene("OptionsMenu");
    }

    // Open main menu.
    public void OpenMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    // TODO
    public void SetSoundFX()
    {
        // To be implemented
    }

    // TODO
    public void SetMusic()
    {
        // To be implemented
    }

    // Dev button to unlock all available levels. (up to 3)
    // Saves PlayerPrefs.
    public void UnlockAllLevels()
    {
        // Unlock all levels
        PlayerPrefs.SetInt("Level01Unlocked", 1);
        PlayerPrefs.SetInt("Level02Unlocked", 1);
        PlayerPrefs.SetInt("Level03Unlocked", 1);
        PlayerPrefs.SetInt("Level04Unlocked", 1);
        PlayerPrefs.SetInt("Level05Unlocked", 1);
        PlayerPrefs.SetInt("Level06Unlocked", 1);
        PlayerPrefs.SetInt("Level07Unlocked", 1);
        PlayerPrefs.SetInt("Level08Unlocked", 1);
        PlayerPrefs.SetInt("Level09Unlocked", 1);
        PlayerPrefs.SetInt("Level10Unlocked", 1);
        PlayerPrefs.SetInt("Level11Unlocked", 1);
        PlayerPrefs.SetInt("Level12Unlocked", 1);
        PlayerPrefs.SetInt("Level13Unlocked", 1);
        PlayerPrefs.SetInt("Level14Unlocked", 1);
        PlayerPrefs.SetInt("Level15Unlocked", 1);
        PlayerPrefs.SetInt("Level16Unlocked", 1);
        PlayerPrefs.SetInt("Level17Unlocked", 1);
        PlayerPrefs.SetInt("Level18Unlocked", 1);
        PlayerPrefs.SetInt("Level19Unlocked", 1);
        PlayerPrefs.SetInt("Level20Unlocked", 1);
        PlayerPrefs.SetInt("Level21Unlocked", 1);
        PlayerPrefs.SetInt("Level22Unlocked", 1);
        PlayerPrefs.SetInt("Level23Unlocked", 1);
        PlayerPrefs.SetInt("Level24Unlocked", 1);
        PlayerPrefs.SetInt("Level25Unlocked", 1);

        PlayerPrefs.Save();
    }

    // Resets level unlock progress.
    // Resets all player statistics then save PlayerPrefs.
    public void ResetProgress()
    {
        // Default Level Unlock Settings
        PlayerPrefs.SetInt("Level01Unlocked", 1);
        PlayerPrefs.SetInt("Level02Unlocked", 0);
        PlayerPrefs.SetInt("Level03Unlocked", 0);
        PlayerPrefs.SetInt("Level04Unlocked", 0);
        PlayerPrefs.SetInt("Level05Unlocked", 0);
        PlayerPrefs.SetInt("Level06Unlocked", 0);
        PlayerPrefs.SetInt("Level07Unlocked", 0);
        PlayerPrefs.SetInt("Level08Unlocked", 0);
        PlayerPrefs.SetInt("Level09Unlocked", 0);
        PlayerPrefs.SetInt("Level10Unlocked", 0);
        PlayerPrefs.SetInt("Level11Unlocked", 0);
        PlayerPrefs.SetInt("Level12Unlocked", 0);
        PlayerPrefs.SetInt("Level13Unlocked", 0);
        PlayerPrefs.SetInt("Level14Unlocked", 0);
        PlayerPrefs.SetInt("Level15Unlocked", 0);
        PlayerPrefs.SetInt("Level16Unlocked", 0);
        PlayerPrefs.SetInt("Level17Unlocked", 0);
        PlayerPrefs.SetInt("Level18Unlocked", 0);
        PlayerPrefs.SetInt("Level19Unlocked", 0);
        PlayerPrefs.SetInt("Level20Unlocked", 0);
        PlayerPrefs.SetInt("Level21Unlocked", 0);
        PlayerPrefs.SetInt("Level22Unlocked", 0);
        PlayerPrefs.SetInt("Level23Unlocked", 0);
        PlayerPrefs.SetInt("Level24Unlocked", 0);
        PlayerPrefs.SetInt("Level25Unlocked", 0);

        // Default Statistics
        for (int i = 1; i < 26; i++)
        {
            if (i < 10)
                ResetLevelStatistics("0" + i);
            else if (i >= 10 && i < 26)
                ResetLevelStatistics(i.ToString());
        }

        PlayerPrefs.Save();
    }

    // Helper method for reseting level statistics based on level number.
    private void ResetLevelStatistics(string levelNumber)
    {
        PlayerPrefs.SetFloat("BestTimeLevel" + levelNumber, 0);
        PlayerPrefs.SetFloat("BestHealthLevel" + levelNumber, 0);
        PlayerPrefs.SetFloat("BestScoreLevel" + levelNumber, 0);
    }
}
