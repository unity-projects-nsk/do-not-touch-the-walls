﻿using UnityEngine;
using TMPro;

public class TimerManager : MonoBehaviour {

    GameManager gm;

    public TextMeshProUGUI timerText;
    bool playerStartedMoving;

    float time = 0f;
    public float speedMultiplier = 1f;

	// Finds GameManger Object.
    // Sets timerText to 00:00 as default.
    // Sets playerMoving to false by default.
	void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        timerText.text = "00:00";
        playerStartedMoving = false;
    }
	
	// If player has not started moving, don't start timer.
    // As soon as player hits any key, timer starts.
    // Timer is simply adding a framerate independent time (with speed mod for dev purposes)
    // and format the time into Hours, Minutes, and Seconds via GameManger TimeFortmatter utility class.
    // Sets Final Time on GameManager to current time every frame.
	void Update () {

        if (Input.anyKeyDown)
            playerStartedMoving = true;

        if (playerStartedMoving)
        {
            time += Time.deltaTime * speedMultiplier;
            timerText.text = gm.TimeFormatter(time);
            gm.finalTime = time;
        }
    }
}
