﻿using UnityEngine;
using TMPro;

public class LevelButtonManager : MonoBehaviour {

    GameManager gm;
    string levelName;
    public TextMeshProUGUI bestTime, bestHealth, bestScore;

    // Find GameManager and instantiated levelName to the current gameObject's name.
    private void Awake()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        levelName = gameObject.name;
    }

    // Call event when Mouse enters this UI element, forwards the gameObject moused over.
    // Sets statistic UI Canvas Group object to max opacity
    // Loads level statistics to be displayed from PlayerPrefs if they exist.
    public void OnPointerEnter(GameObject level)
    {
        gameObject.GetComponent<CanvasGroup>().alpha = 1f;
        levelName = level.name;
        LoadLevelStatistics();
    }

    // Call event when Mouse exits this UI element.
    // Sets statistic UI Canvas Group object to max transparency
    public void OnPointerExit()
    {
        gameObject.GetComponent<CanvasGroup>().alpha = 0;
    }

    // Loads level statistics, and sets them to relevant TMPro UI text fields from PlayerPrefs.
    private void LoadLevelStatistics()
    {
        bestTime.text = gm.TimeFormatter(PlayerPrefs.GetFloat("BestTimeLevel" + levelName));
        bestHealth.text = PlayerPrefs.GetFloat("BestHealthLevel" + levelName).ToString("0.");
        bestScore.text = PlayerPrefs.GetFloat("BestScoreLevel" + levelName).ToString("0.");

        //Debug.Log(gm.TimeFormatter(PlayerPrefs.GetFloat("BestTimeLevel" + levelName)));
        //Debug.Log(PlayerPrefs.GetFloat("BestHealthLevel" + levelName).ToString("0."));
        //Debug.Log(PlayerPrefs.GetFloat("BestScoreLevel" + levelName).ToString("0."));
    }
}