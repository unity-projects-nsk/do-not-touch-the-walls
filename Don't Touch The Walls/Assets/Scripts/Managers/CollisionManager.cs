﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionManager : MonoBehaviour {

    GameManager gm;
    public float healthLossPerSecond = 10.0f;
    
    // Finds GameManger object.
    private void Update()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // When player object hits wall or stays in contact with wall, subtract 10 health per second
    // multiplied by Time.deltaTime to make it framerate independent.
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Wall"))
        {
            gm.playerHealth -= healthLossPerSecond * Time.deltaTime;
        }
    }

    // When player reaches level end trigger, set gameover to true and unlock next level.
    // Returns to Level Select after hardcoded 1.5 seconds.
    // Only first 3 levels are available in this version of the game.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("EndTrigger"))
        {
            gm.isGameOver = true;

            // 1. Unlocks next level dependent on the level you just completed.
            // 2. Saves Best Time, Highest Health, and Overall Calculated Score.
            if (SceneManager.GetActiveScene().name.Equals("Level01"))
            {
                PlayerPrefs.SetInt("Level02Unlocked", 1);
                SaveLevelStatistics("Level01");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level02"))
            {
                PlayerPrefs.SetInt("Level03Unlocked", 1);
                SaveLevelStatistics("Level02");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level03"))
            {
                PlayerPrefs.SetInt("Level04Unlocked", 1);
                SaveLevelStatistics("Level03");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level04"))
            {
                PlayerPrefs.SetInt("Level05Unlocked", 1);
                SaveLevelStatistics("Level04");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level05"))
            {
                PlayerPrefs.SetInt("Level06Unlocked", 1);
                SaveLevelStatistics("Level05");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level06"))
            {
                PlayerPrefs.SetInt("Level07Unlocked", 1);
                SaveLevelStatistics("Level06");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level07"))
            {
                PlayerPrefs.SetInt("Level08Unlocked", 1);
                SaveLevelStatistics("Level07");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level08"))
            {
                PlayerPrefs.SetInt("Level09Unlocked", 1);
                SaveLevelStatistics("Level08");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level09"))
            {
                PlayerPrefs.SetInt("Level10Unlocked", 1);
                SaveLevelStatistics("Level09");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level10"))
            {
                PlayerPrefs.SetInt("Level11Unlocked", 1);
                SaveLevelStatistics("Level10");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level11"))
            {
                PlayerPrefs.SetInt("Level12Unlocked", 1);
                SaveLevelStatistics("Level11");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level12"))
            {
                PlayerPrefs.SetInt("Level13Unlocked", 1);
                SaveLevelStatistics("Level12");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level13"))
            {
                PlayerPrefs.SetInt("Level14Unlocked", 1);
                SaveLevelStatistics("Level13");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level14"))
            {
                PlayerPrefs.SetInt("Level15Unlocked", 1);
                SaveLevelStatistics("Level14");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level15"))
            {
                PlayerPrefs.SetInt("Level16Unlocked", 1);
                SaveLevelStatistics("Level15");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level16"))
            {
                PlayerPrefs.SetInt("Level17Unlocked", 1);
                SaveLevelStatistics("Level16");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level17"))
            {
                PlayerPrefs.SetInt("Level18Unlocked", 1);
                SaveLevelStatistics("Level17");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level18"))
            {
                PlayerPrefs.SetInt("Level19Unlocked", 1);
                SaveLevelStatistics("Level18");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level19"))
            {
                PlayerPrefs.SetInt("Level20Unlocked", 1);
                SaveLevelStatistics("Level19");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level20"))
            {
                PlayerPrefs.SetInt("Level21Unlocked", 1);
                SaveLevelStatistics("Level20");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level21"))
            {
                PlayerPrefs.SetInt("Level22Unlocked", 1);
                SaveLevelStatistics("Level21");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level22"))
            {
                PlayerPrefs.SetInt("Level23Unlocked", 1);
                SaveLevelStatistics("Level22");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level23"))
            {
                PlayerPrefs.SetInt("Level24Unlocked", 1);
                SaveLevelStatistics("Level23");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level24"))
            {
                PlayerPrefs.SetInt("Level25Unlocked", 1);
                SaveLevelStatistics("Level24");
            }
            if (SceneManager.GetActiveScene().name.Equals("Level25"))
            {
                SaveLevelStatistics("Level25");
            }

            StartCoroutine(LoadLevelSelect());
        }
    }

    // Saves level statistics upon beating a level if statistics are better than previous statistics.
    // Saved to PlayerPrefs dynamically based on inputed level name.
    void SaveLevelStatistics(string level)
    {
        if (gm.finalTime < PlayerPrefs.GetFloat("BestTime" + level) || PlayerPrefs.GetFloat("BestTime" + level) == 0)
            PlayerPrefs.SetFloat("BestTime" + level, gm.finalTime);
        if (gm.playerHealth > PlayerPrefs.GetFloat("BestHealth" + level))
            PlayerPrefs.SetFloat("BestHealth" + level, gm.playerHealth);
        if (((gm.playerHealth * 2) - gm.finalTime) > PlayerPrefs.GetFloat("BestScore" + level))
            PlayerPrefs.SetFloat("BestScore" + level, (gm.playerHealth * 2) - gm.finalTime);
        PlayerPrefs.Save();
    }

    // Coroutine to allow for a delay before returning player to Level Select after beating level.
    IEnumerator LoadLevelSelect()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        SceneManager.LoadScene("LevelSelectMenu");
    }
}
