﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour {

    public Button[] levels;

    // Sets levels unlock status to PlayerPrefs values using int to bool tertiary conditionals.
    private void Awake()
    {
        levels[0].interactable = PlayerPrefs.GetInt("Level01Unlocked") == 1 ? true : false;
        levels[1].interactable = PlayerPrefs.GetInt("Level02Unlocked") == 1 ? true : false;
        levels[2].interactable = PlayerPrefs.GetInt("Level03Unlocked") == 1 ? true : false;
        levels[3].interactable = PlayerPrefs.GetInt("Level04Unlocked") == 1 ? true : false;
        levels[4].interactable = PlayerPrefs.GetInt("Level05Unlocked") == 1 ? true : false;
        levels[5].interactable = PlayerPrefs.GetInt("Level06Unlocked") == 1 ? true : false;
        levels[6].interactable = PlayerPrefs.GetInt("Level07Unlocked") == 1 ? true : false;
        levels[7].interactable = PlayerPrefs.GetInt("Level08Unlocked") == 1 ? true : false;
        levels[8].interactable = PlayerPrefs.GetInt("Level09Unlocked") == 1 ? true : false;
        levels[9].interactable = PlayerPrefs.GetInt("Level10Unlocked") == 1 ? true : false;
        levels[10].interactable = PlayerPrefs.GetInt("Level11Unlocked") == 1 ? true : false;
        levels[11].interactable = PlayerPrefs.GetInt("Level12Unlocked") == 1 ? true : false;
        levels[12].interactable = PlayerPrefs.GetInt("Level13Unlocked") == 1 ? true : false;
        levels[13].interactable = PlayerPrefs.GetInt("Level14Unlocked") == 1 ? true : false;
        levels[14].interactable = PlayerPrefs.GetInt("Level15Unlocked") == 1 ? true : false;
        levels[15].interactable = PlayerPrefs.GetInt("Level16Unlocked") == 1 ? true : false;
        levels[16].interactable = PlayerPrefs.GetInt("Level17Unlocked") == 1 ? true : false;
        levels[17].interactable = PlayerPrefs.GetInt("Level18Unlocked") == 1 ? true : false;
        levels[18].interactable = PlayerPrefs.GetInt("Level19Unlocked") == 1 ? true : false;
        levels[19].interactable = PlayerPrefs.GetInt("Level20Unlocked") == 1 ? true : false;
        levels[20].interactable = PlayerPrefs.GetInt("Level21Unlocked") == 1 ? true : false;
        levels[21].interactable = PlayerPrefs.GetInt("Level22Unlocked") == 1 ? true : false;
        levels[22].interactable = PlayerPrefs.GetInt("Level23Unlocked") == 1 ? true : false;
        levels[23].interactable = PlayerPrefs.GetInt("Level24Unlocked") == 1 ? true : false;
        levels[24].interactable = PlayerPrefs.GetInt("Level25Unlocked") == 1 ? true : false;
    }

    // Public class for use by level button's OnClick() event.
    // Takes in level number and dynamically loads the correct level.
    public void LevelSelect (int levelNumber)
    {
        if (levelNumber < 10 && levelNumber > 0)
            SceneManager.LoadScene("Level0" + levelNumber);
        if (levelNumber > 9)
            SceneManager.LoadScene("Level" + levelNumber);
    }
    
}
