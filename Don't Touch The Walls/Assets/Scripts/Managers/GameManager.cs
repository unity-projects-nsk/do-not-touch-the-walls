﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour {

    bool isHoursActive = false;

    public float playerHealth = 100f;

    public TextMeshProUGUI healthText;
    public TextMeshProUGUI GameOverText;
    public bool isGameOver;
    public float finalTime;

    // Set TimeScale back to 1 on start to prevent locked game.
    // If the active scene is a level, lock the cursor to center of screen and make
    // it invisible with the LockCursor() utility method.
    // Set gameover to false and disable game over text.
    private void Start()
    {
        Time.timeScale = 1;

        if (SceneManager.GetActiveScene().name.Contains("Level") && !SceneManager.GetActiveScene().name.Contains("LevelSelect"))
        {
            LockCursor();
            isGameOver = false;
            GameOverText.enabled = false;
        }
    }

    // Set HP to playerHealth variable, formatted to get rid of decimals.
    // Set game over to true if health reaches zero.
    // If game over is true but player has health, player beat level. Pause game, unlock cursor.
    // If game over is true and player has no health, player lost. Pause game, unlock cursor.
    // Constantly check for R key input for Restarting and Escape key input for returning to main menu.
    private void Update()
    {
        healthText.text = "HP: " + playerHealth.ToString("0.");

        if (playerHealth <= 0)
        {
            isGameOver = true;
        }

        if(isGameOver && playerHealth > 0)
        {
            GameOverText.text = "You completed the maze!\nCompletion Time: " + TimeFormatter(finalTime) + "\nCongratulations!";
            GameOverText.enabled = true;
            Time.timeScale = 0;
            UnlockCursor();

        }
        else if (isGameOver && playerHealth <=0)
        {
            GameOverText.text = "You Died.\nPress R to Restart Level.\nOr Escape to Return to Main Menu.";
            GameOverText.enabled = true;
            Time.timeScale = 0;
            UnlockCursor();
        }

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //if (Input.GetKeyDown(KeyCode.Escape))
        //    SceneManager.LoadScene("MainMenu");
    }

    // Utility method for locking cursor and making it invisible.
    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Utility method for unlocking cursor and making it visible.
    private void UnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    
    // Formatter for float time variable (in seconds) to a hh:mm:ss format.
    public string TimeFormatter(float time)
    {
        string finalTime = "00:00";
        float sec = Mathf.Floor(time % 60);
        float min = Mathf.Floor((time % 3600) / 60);
        float hr = Mathf.Floor((time % 216000) / 3600);

        if (min >= 59 && sec >= 59)
            isHoursActive = true;
        if (!isHoursActive)
            finalTime = min.ToString("00") + ":" + sec.ToString("00");
        if (isHoursActive)
            finalTime = hr.ToString("00") + ":" + min.ToString("00") + ":" + sec.ToString("00");
        return finalTime;
    }

}
