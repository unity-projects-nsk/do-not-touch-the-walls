﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    Transform player;
    public Vector3 offset;

    // Hardcodes offset and sets player equal to the instantiated player object that is created
    // by the LevelGenerator script.
    private void Start()
    {
        offset = new Vector3(-0.5f, 6f, 0);
        player = GameObject.Find("Player(Clone)").transform.GetChild(0).transform;
    }

    // Sets camera to player position added to offset.
    private void Update()
    {
        gameObject.transform.position = player.position + offset;
    }
}
