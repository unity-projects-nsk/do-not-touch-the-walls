﻿using UnityEngine;

[System.Serializable]
public class ColorMappings
{
    public Color color;
    public GameObject prefab;
}
