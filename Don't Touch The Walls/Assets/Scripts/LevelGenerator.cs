﻿using UnityEngine;

// This class dynamically generates levels via a color to prefab instantiation system.
// This allows for quick level creation by importing a PSD file with the colors outlined
// in the level color mappings. Currently only supports the player spawn, the end goal, and walls.

public class LevelGenerator : MonoBehaviour {
    
    GameObject floor;
    public Material floorMat;
    public Texture2D levelMap;
    public ColorMappings[] colorMappings;

    // Calls constructor methods for building the maze and floor.
	void Awake () {
        GenerateMaze();
        GenerateFloor();
    }

    // Custom method for generating a primitive plane.
    // Dynamically sets plane size and position to be appropriate for loaded level layout.
    // Sets floor material to assigned material.
    void GenerateFloor()
    {
        floor = GameObject.CreatePrimitive(PrimitiveType.Plane);
        floor.transform.localScale = new Vector3(levelMap.width / 10f, 1, levelMap.height / 10f);
        floor.transform.position = new Vector3((levelMap.width / 10f) * 5f, 0, -(levelMap.height / 10f) * 5f);
        floor.GetComponent<MeshRenderer>().material = floorMat;
    }

    // Dynamically creates maze via an imported image.
    // This uses a nested for loop to generate a block based on
    // the color of the individual pixels inside the levelMap Texture2D.
    // This then calls the GenerateBlock() method at every pixel and generates
    // a prefab at these positions.
    void GenerateMaze()
    {
        for (int x = 0; x < levelMap.width; x++)
        {
            for (int y = 0; y < levelMap.height; y++)
            {
                GenerateBlock(x, y);
            }
        }
    }

    // Generates a prefab at the given x and y values based on the pixel color of the levelMap Texture2D.
    // Skips pixels that have an alpha of 0 (pixels that are completely transparent.)
    // Loops through every single color to prefab mapping, compares the color of the levelMap pixel to the colorMappings
    // and instantiates a prefab associated with that color into worldspace.
    void GenerateBlock(int x, int y)
    {
        Color pixel = levelMap.GetPixel(x, y);

        if (pixel.a == 0)
            return;

        foreach (ColorMappings colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixel))
            {
                Vector3 pos = new Vector3(x, 0, -y);

                if (colorMapping.prefab.Equals(colorMappings[1].prefab))
                    Instantiate(colorMapping.prefab, pos, Quaternion.identity);
                else
                    Instantiate(colorMapping.prefab, pos, Quaternion.identity, transform);
            }
        }
    }
}
