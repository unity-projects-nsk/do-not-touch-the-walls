﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    Rigidbody player;

    public int playerSpeed = 10;
    public int jumpForce = 300;
    public int lookSensitivity = 2;

	// Sets player to current gameobject's rigidbody.
	void Start () {
        player = gameObject.GetComponent<Rigidbody>();
	}
	
	// Movement for WASD. Jumping currently unneeded and disabled.
	void Update () {

        // Vector3(LEFT/RIGHT, UP/DOWN, NORTH/SOUTH)

        // Forward
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            player.AddForce(1 * Time.deltaTime * playerSpeed, 0, 0);
        // Backward
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            player.AddForce(-1 * Time.deltaTime * playerSpeed, 0, 0);
        // Right
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            player.AddForce(0, 0, -1 * Time.deltaTime * playerSpeed);
        // Left
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            player.AddForce(0, 0, 1 * Time.deltaTime * playerSpeed);

        // Jump
        //if (Input.GetKeyDown(KeyCode.Space) && player.transform.position.y <= 1.0f)
        //    player.AddForce(new Vector3(0, 1, 0) * jumpForce);
        
    }
}
