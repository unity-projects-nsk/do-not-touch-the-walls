﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody2D rb;
    RaycastHit2D hit;
    Animator anim;
    bool FacingRight;
    bool IsGrounded;

    public float moveForce = 365f;
    public float maxHorizontalVelocity = 7f;
    public float jumpForce = 2f;
    
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        FacingRight = true;
	}

    private void Update()
    {
        hit = Physics2D.Raycast(transform.position, Vector2.down, 0.6f);
        Debug.DrawRay(transform.position, new Vector2(0 , -0.6f), Color.red);

        if (Input.GetKeyDown(KeyCode.Space) && hit.collider != null)
        {
            if (hit.collider.tag == "Ground")
                IsGrounded = true;
            else
                IsGrounded = false;
        }

    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        if (horizontal > 0 && !FacingRight)
            Flip();
        else if (horizontal < 0 && FacingRight)
            Flip();

        if (horizontal * rb.velocity.x < maxHorizontalVelocity)
            rb.AddForce(Vector2.right * horizontal * moveForce);

        if (Mathf.Abs(rb.velocity.x) > 0)
            anim.SetBool("IsWalking", true);
        else
            anim.SetBool("IsWalking", false);

        if (IsGrounded)
        {
            anim.SetBool("IsJumping", true);
            rb.AddForce(new Vector2(0, jumpForce));
            IsGrounded = false;
            //anim.SetBool("IsJumping", false);
        }
	}

    void Flip()
    {
        FacingRight = !FacingRight;
        Vector3 scale = transform.localScale;
        scale.x = -scale.x;
        transform.localScale = scale;
    }
}
